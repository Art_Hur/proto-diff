# wtcp-diff

# Usage
```
wtcp-diff -dump [path to an input wakfu binaries JAR file] [path to an output YAML file]       
wtcp-diff -diff [path to an input source Wakfu binaries JAR or a YAML file] [path to an input destination Wakfu binaries JAR or a YAML file] 
```

# How to build
Build depends on:

a) if you use nix

- stack
- nix

b) if you don't use nix

- stack
- git
- zlib

If you don't use nix, disable it in the `stack.yaml` file by setting `enable: false`.

Then compile the project using stack:
```
stack setup
stack build
```

And you should be able to run the executable:
```
stack exec wtcp-diff -- -dump /path/to/binaries.jar ./output.yaml
```

# Example output
![differ.png](https://bitbucket.org/repo/jgEA9eX/images/3820788155-differ.png)