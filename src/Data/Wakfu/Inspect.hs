{-# LANGUAGE DeriveGeneric #-}
module Data.Wakfu.Inspect
    ( Message(..)
    , Member(..)
    , MemberType(..)
    , inspectMessageStructures
    ) where
import qualified Codec.Archive.Zip    as Zip
import           Control.Foldl        (Fold)
import qualified Control.Foldl        as L
import           Data.Aeson           (FromJSON, ToJSON (..), defaultOptions,
                                       genericToEncoding)
import           Data.Binary.Get      (runGet)
import qualified Data.ByteString.Lazy as BL
import           Data.Char            (isAlphaNum)
import           Data.Foldable        (toList)
import           Data.List            (find)
import           Data.Maybe           (fromJust, fromMaybe, isJust, maybeToList)
import           Data.Monoid          (All (..), (<>))
import           Data.Sequence        (Seq, (|>))
import qualified Data.Sequence        as Seq
import           GHC.Generics         (Generic)
import           Language.JVM.CFG     (bbByPC, bbInsts)
import qualified Language.JVM.Parser  as JVM
import           System.FilePath

data Message =
    Message { messageId      :: !Int
            , obfuscatedName :: !String
            , dataMembers    :: ![Member]
            } deriving(Generic, Show)

data Member =
    Member { memberName :: Maybe String
           , memberType :: !MemberType
           } deriving(Generic, Show)

data MemberType
    = Boolean
    | Byte
    | Int16
    | Int32
    | Int64
    | Float32
    | Float64
    | Str
    | Array !MemberType
    | HashMap !MemberType !MemberType
    | Composite ![MemberType]
    deriving(Generic, Show, Eq)

instance FromJSON Message

instance ToJSON Message where
    toEncoding = genericToEncoding defaultOptions

instance FromJSON Member

instance ToJSON Member where
    toEncoding = genericToEncoding defaultOptions

instance FromJSON MemberType

instance ToJSON MemberType where
    toEncoding = genericToEncoding defaultOptions

type ClassAccess = (String -> Maybe JVM.Class)

compositeRecursionLevel :: Int
compositeRecursionLevel = 2

inspectMessageStructures :: FilePath -> IO ([Message], [Message])
inspectMessageStructures path = do
    contents <- BL.readFile path
    let archive = Zip.toArchive contents
        classEntries = filter ((== ".class") . takeExtension . Zip.eRelativePath) $ Zip.zEntries archive
        classLoader nm = readClass <$> find ((== nm) . stripClassName) classEntries
    return $ L.fold (L.fold (collectMessageClasses classLoader) classEntries) classEntries

collectMessageClasses :: ClassAccess -> Fold Zip.Entry (Fold Zip.Entry ([Message], [Message]))
collectMessageClasses classLoader =
    L.premap readClass $
        collectSubclasses <$>
            L.find isInputOnlyMessageBase <*>
            L.find isOutputOnlyMessageBase
    where
        collectSubclasses :: Maybe JVM.Class -> Maybe JVM.Class -> Fold Zip.Entry ([Message], [Message])
        collectSubclasses (Just inputBase) (Just outputBase) =
            let inputBaseName = JVM.className inputBase
                outputBaseName = JVM.className outputBase
            in L.premap readClass $ L.foldMap (foldSubclasses inputBaseName outputBaseName) id
        collectSubclasses Nothing _ = error "couldn't find the input message base class!"
        collectSubclasses _ Nothing = error "couldn't find the output message base class!"
        foldSubclasses :: String -> String -> JVM.Class -> ([Message], [Message])
        foldSubclasses inputBase outputBase cls
            | JVM.superClass cls == Just inputBase = (maybeToList $ toMessage cls, [])
            | JVM.superClass cls == Just outputBase = ([], maybeToList $ toMessage cls)
            | otherwise = ([], [])
        toMessage :: JVM.Class -> Maybe Message
        toMessage cls =
            flip fmap (messageClassId cls) $ \mid ->
                Message mid (JVM.className cls) $
                    Member Nothing <$> classDataMembers compositeRecursionLevel classLoader cls
        isInputOnlyMessageBase :: JVM.Class -> Bool
        isInputOnlyMessageBase = getAll . isInputOnlyMessageBase'
            where
                isInputOnlyMessageBase' =
                    (All . JVM.classIsAbstract) <>
                    (All . isJust . JVM.superClass) <>
                    (All . (== 2) . length . JVM.classMethods) <>
                    (All . any (getAll . isEncodeMethod) . JVM.classMethods)
                isEncodeMethod =
                    (All . (== Just (JVM.ArrayType JVM.ByteType)) . JVM.methodReturnType) <>
                    (All . null . JVM.methodParameterTypes)
        isOutputOnlyMessageBase :: JVM.Class -> Bool
        isOutputOnlyMessageBase = getAll . isOutputOnlyMessageBase'
            where
                isOutputOnlyMessageBase' =
                    (All . JVM.classIsAbstract) <>
                    (All . isJust . JVM.superClass) <>
                    (All . (== 2) . length . JVM.classMethods) <>
                    (All . any (getAll . isDecodeMethod) . JVM.classMethods)
                isDecodeMethod =
                    (All . (== Just JVM.BooleanType) . JVM.methodReturnType) <>
                    (All . (== 1) . length . JVM.methodParameterTypes) <>
                    (All . (== JVM.ArrayType JVM.ByteType) . head . JVM.methodParameterTypes)

-- TODO: implement this properly (by looking up the name of the id method on the base interface)
messageClassId :: JVM.Class -> Maybe Int
messageClassId cls =
    fmap fromJust
      . find isJust
      . fmap parseMessageIdMethod
      . filter (getAll . canBeMessageIdMethod) $ JVM.classMethods cls
      where
        parseMessageIdMethod m =
            case JVM.methodBody m of
                (JVM.Code _ _ cfg _ _ _ _) ->
                    let bb = bbByPC cfg 0
                    in case fmap (fmap snd . bbInsts) bb of
                        (Just [JVM.Ldc (JVM.Integer v), JVM.Ireturn]) ->
                            Just $ fromIntegral v
                        _ -> Nothing
                _ -> Nothing
        canBeMessageIdMethod =
            (All . (== Just JVM.IntType) . JVM.methodReturnType) <>
            (All . null . JVM.methodParameterTypes)

classDataMembers :: Int -> ClassAccess -> JVM.Class -> [MemberType]
classDataMembers recurse classLoader =
    fmap (fromJVM <$> JVM.fieldType <*> JVM.fieldSignature)
        . filter ((== JVM.Private) . JVM.fieldVisibility) . JVM.classFields
    where
        fromJVM :: JVM.Type -> Maybe String -> MemberType
        fromJVM JVM.ShortType _ = Int16
        fromJVM JVM.LongType _ = Int64
        fromJVM JVM.IntType _ = Int32
        fromJVM JVM.FloatType _ = Float32
        fromJVM JVM.DoubleType _ = Float64
        fromJVM JVM.ByteType _ = Byte
        fromJVM JVM.CharType _ = Byte
        fromJVM JVM.BooleanType _ = Boolean
        fromJVM (JVM.ArrayType mt) _ = Array $ fromJVM mt Nothing
        fromJVM (JVM.ClassType nm) Nothing = fromSignature . parseSignature $ "L" ++ nm ++ ";"
        fromJVM (JVM.ClassType _) (Just sig) = fromSignature $ parseSignature sig
        fromSignature :: Signature -> MemberType
        fromSignature (SigRef "java/lang/String" []) = Str
        fromSignature (SigRef "java/lang/Boolean" []) = Boolean
        fromSignature (SigPrim 'Z') = Boolean
        fromSignature (SigRef "java/lang/Long" []) = Int64
        fromSignature (SigPrim 'J') = Int64
        fromSignature (SigRef "java/lang/Integer" []) = Int32
        fromSignature (SigPrim 'I') = Int32
        fromSignature (SigRef "java/lang/Short" []) = Int16
        fromSignature (SigPrim 'S') = Int16
        fromSignature (SigRef "java/lang/Byte" []) = Byte
        fromSignature (SigPrim 'B') = Byte
        fromSignature (SigRef "java/lang/Double" []) = Float64
        fromSignature (SigPrim 'D') = Float64
        fromSignature (SigRef "java/lang/Float" []) = Float32
        fromSignature (SigPrim 'F') = Float32
        fromSignature (SigArray sig) = Array $ fromSignature sig
        -- classes used to store data in the game
        fromSignature (SigRef "java/util/HashMap" [k, v]) =
            HashMap (fromSignature k) (fromSignature v)
        fromSignature (SigRef "java/util/List" [m]) = Array $ fromSignature m
        fromSignature (SigRef "java/util/ArrayList" [m]) = Array $ fromSignature m
        fromSignature (SigRef "gnu/trove/list/array/TByteArrayList" []) = Array Byte
        fromSignature (SigRef "gnu/trove/list/array/TShortArrayList" []) = Array Int16
        fromSignature (SigRef "gnu/trove/list/array/TIntArrayList" []) = Array Int32
        fromSignature (SigRef "gnu/trove/list/array/TLongArrayList" []) = Array Int64
        fromSignature (SigRef "gnu/trove/list/array/TFloatArrayList" []) = Array Float32
        fromSignature (SigRef "gnu/trove/map/hash/TByteObjectHashMap" [v]) =
            HashMap Byte $ fromSignature v
        fromSignature (SigRef "gnu/trove/map/hash/TShortObjectHashMap" [v]) =
            HashMap Int16 $ fromSignature v
        fromSignature (SigRef "gnu/trove/map/hash/TIntObjectHashMap" [v]) =
            HashMap Int32 $ fromSignature v
        fromSignature (SigRef "gnu/trove/map/hash/TLongObjectHashMap" [v]) =
            HashMap Int64 $ fromSignature v
        fromSignature (SigRef "gnu/trove/map/hash/TFloatObjectHashMap" [v]) =
            HashMap Float32 $ fromSignature v
        fromSignature (SigRef "gnu/trove/map/hash/TObjectByteHashMap" [k]) =
            flip HashMap Byte $ fromSignature k
        fromSignature (SigRef "gnu/trove/map/hash/TObjectShortHashMap" [k]) =
            flip HashMap Int16 $ fromSignature k
        fromSignature (SigRef "gnu/trove/map/hash/TObjectIntHashMap" [k]) =
            flip HashMap Int32 $ fromSignature k
        fromSignature (SigRef "gnu/trove/map/hash/TObjectLongHashMap" [k]) =
            flip HashMap Int64 $ fromSignature k
        fromSignature (SigRef "gnu/trove/map/hash/TObjectFloatHashMap" [k]) =
            flip HashMap Float32 $ fromSignature k
        fromSignature (SigRef "gnu/trove/map/hash/TIntByteHashMap" []) = HashMap Int32 Byte
        fromSignature (SigRef "gnu/trove/map/hash/TIntShortHashMap" []) = HashMap Int32 Int16
        fromSignature (SigRef "gnu/trove/map/hash/TIntIntHashMap" []) = HashMap Int32 Int32
        fromSignature (SigRef "gnu/trove/map/hash/TIntLongHashMap" []) = HashMap Int32 Int64
        fromSignature (SigRef "gnu/trove/map/hash/TIntFloatHashMap" []) = HashMap Int32 Float32
        fromSignature (SigRef "gnu/trove/map/hash/TByteIntHashMap" []) = HashMap Byte Int32
        fromSignature (SigRef "gnu/trove/map/hash/TShortIntHashMap" []) = HashMap Int16 Int32
        fromSignature (SigRef "gnu/trove/map/hash/TLongIntHashMap" []) = HashMap Int64 Int32
        fromSignature (SigRef "gnu/trove/map/hash/TFloatIntHashMap" []) = HashMap Float32 Int32
        fromSignature (SigRef "java/util/concurrent/atomic/AtomicReference" [v]) = fromSignature v
        fromSignature (SigRef nm []) =
            if recurse > 0 then
               Composite . fromMaybe [] $ fmap (classDataMembers (recurse - 1) classLoader) (classLoader nm)
            else Composite []
        fromSignature _ = Composite []

data Signature
    = SigArray Signature
    | SigRef String [Signature]
    | SigPrim Char
    | SigTypeVar String
    deriving (Show)

-- a simple JVM signature parser
parseSignature :: String -> Signature
parseSignature = fst . parseSignature'
    where
        parseSignature' :: String -> (Signature, String)
        parseSignature' ('[':xs) =
            let (sig, xss) = parseSignature' xs
            in (SigArray sig, xss)
        parseSignature' ('+':xs) = parseSignature' xs
        parseSignature' ('-':xs) = parseSignature' xs
        parseSignature' ('T':xs) =
            let (nm, ';':xss) = span isAlphaNum xs
            in (SigTypeVar nm, xss)
        parseSignature' ('L':xs) =
            case span (\c -> isAlphaNum c || c == '/') xs of
                (x, '<':xss) ->
                    let (args, ';':xsss) = collectArgs (Seq.empty, xss)
                    in (SigRef x (toList args), xsss)
                (x, ';':xss) -> (SigRef x [], xss)
        parseSignature' (c:xs)
            | isPrimitiveSignature c = (SigPrim c, xs)
            | otherwise = error "failed to parse a type signature"
        isPrimitiveSignature :: Char -> Bool
        isPrimitiveSignature c = c `elem` ['Z', 'B', 'C', 'S', 'I', 'J', 'F', 'D']
        collectArgs :: (Seq Signature, String) -> (Seq Signature, String)
        collectArgs (sigs, '>':xs) = (sigs, xs)
        collectArgs (sigs, str) =
            let (sig, xs) = parseSignature' str
            in collectArgs (sigs |> sig, xs)

readClass :: Zip.Entry -> JVM.Class
{-# INLINE readClass #-}
readClass = runGet JVM.getClass . Zip.fromEntry

stripClassName :: Zip.Entry -> String
{-# INLINE stripClassName #-}
stripClassName = takeBaseName . Zip.eRelativePath
